package com.qwertovsky.ovsd45notifier.plugin.filter;

import static org.junit.Assert.*;

import java.util.ArrayList;
import java.util.List;

import org.apache.log4j.BasicConfigurator;
import org.apache.log4j.Logger;

import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

import com.qwertovsky.ovsd45notifier.plugin.ServiceCall;
import com.qwertovsky.ovsd45notifier.plugin.filter.ServiceCallImpl;

public class FilterMatchTest
{
	static ServiceCall serviceCall;
	static FilterPlugin plugin;
	
	@BeforeClass
	public static void beforeClass()
	{
		plugin = new FilterPlugin();
		Logger logger = Logger.getLogger("test");
		BasicConfigurator.configure();
		
		plugin.setLogger(logger);
		
		serviceCall = new ServiceCallImpl();
	}
	
	// -------------------------------------------
	@Before
	public void before()
	{
		FilterService.getFilters().clear();
	}
	
	// -------------------------------------------
	@Test
	public void testFilterMatchAny()
	{
		Filter filter = new Filter();
		filter.setInpact(Inpact.ANY_CONDITION);
		FilterService.addFilter(filter);
		List<Condition> conditions = new ArrayList<Condition>();
		
		Condition condition = new Condition(Field.CALLER, Relation.EQUAL);
		condition.setText("");
		conditions.add(condition);
		filter.setConditions(conditions);
		assertFalse(filter.isMatch(serviceCall));
		
		condition = new Condition(Field.CALLER, Relation.EQUAL);
		condition.setText("John Doe");
		conditions.add(condition);
		filter.setConditions(conditions);
		assertTrue(filter.isMatch(serviceCall));
	}
	
	// -------------------------------------------
	@Test
	public void testFilterMatchAll()
	{
		Filter filter = new Filter();
		filter.setInpact(Inpact.ALL_CONDITIONS);
		FilterService.addFilter(filter);
		List<Condition> conditions = new ArrayList<Condition>();
		
		Condition condition = new Condition(Field.CALLER, Relation.CONTAINS);
		condition.setText("Doe");
		conditions.add(condition);
		filter.setConditions(conditions);
		assertTrue(filter.isMatch(serviceCall));
		
		condition = new Condition(Field.CALLER, Relation.EQUAL);
		condition.setText("John Doe");
		conditions.add(condition);
		filter.setConditions(conditions);
		assertTrue(filter.isMatch(serviceCall));
		
		condition = new Condition(Field.CALLER, Relation.EQUAL);
		condition.setText("");
		conditions.add(condition);
		filter.setConditions(conditions);
		assertFalse(filter.isMatch(serviceCall));
	}
	
	// -------------------------------------------
	@Test
	public void testEmptyFilter()
	{
		Filter filter = new Filter();
		filter.setInpact(Inpact.ALL_CONDITIONS);
		FilterService.addFilter(filter);
		List<Condition> conditions = new ArrayList<Condition>();
		
		Condition condition = new Condition(Field.CALLER, Relation.CONTAINS);
//		condition.setText("");
		conditions.add(condition);
		filter.setConditions(conditions);
		assertFalse(filter.isMatch(serviceCall));
	}
	
	// -------------------------------------------
	@Test
	public void testFilterRelations() throws Exception
	{
		Filter filter = new Filter();
		filter.setInpact(Inpact.ALL_CONDITIONS);
		FilterService.addFilter(filter);
		List<Condition> conditions = new ArrayList<Condition>();
		
		Condition condition = new Condition(Field.CALLER, Relation.EQUAL);
		condition.setText("John Doe");
		conditions.add(condition);
		filter.setConditions(conditions);
		assertTrue(filter.isMatch(serviceCall));
		
		condition = new Condition(Field.CALLER, Relation.NOT_EQUAL);
		condition.setText("John Doe2");
		conditions.add(condition);
		filter.setConditions(conditions);
		assertTrue(filter.isMatch(serviceCall));
		
		condition = new Condition(Field.CALLER, Relation.CONTAINS);
		condition.setText(" D");
		conditions.add(condition);
		filter.setConditions(conditions);
		assertTrue(filter.isMatch(serviceCall));
		
		condition = new Condition(Field.CALLER, Relation.END_WITH);
		condition.setText("Doe");
		conditions.add(condition);
		filter.setConditions(conditions);
		assertTrue(filter.isMatch(serviceCall));
		
		condition = new Condition(Field.CALLER, Relation.NOT_CONTAINS);
		condition.setText(" S");
		conditions.add(condition);
		filter.setConditions(conditions);
		assertTrue(filter.isMatch(serviceCall));
		
		condition = new Condition(Field.CALLER, Relation.START_WITH);
		condition.setText("John");
		conditions.add(condition);
		filter.setConditions(conditions);
		assertTrue(filter.isMatch(serviceCall));
	}
	
	// -------------------------------------------
	@Test
	public void testFilterAllConditions() throws Exception
	{
		Filter filter = new Filter();
		filter.setInpact(Inpact.ALL_CONDITIONS);
		FilterService.addFilter(filter);
		List<Condition> conditions = new ArrayList<Condition>();
		
		Condition condition = new Condition(Field.CALLER, Relation.EQUAL);
		condition.setText("JohnDoe");
		conditions.add(condition);
		filter.setConditions(conditions);
		
		condition = new Condition(Field.CALLER, Relation.NOT_EQUAL);
		condition.setText("John Doe2");
		conditions.add(condition);
		filter.setConditions(conditions);
		assertFalse(filter.isMatch(serviceCall));
	}
	
	// -------------------------------------------
	@Test
	public void testProcessServiceCallFields() throws Exception
	{
		Filter filter = new Filter();
		FilterService.addFilter(filter);
		List<Condition> conditions = new ArrayList<Condition>();
		Condition condition;
		condition = new Condition(Field.DESCRIPTION, Relation.EQUAL);
		condition.setText("Description");
		conditions.add(condition);
		filter.setConditions(conditions);
		assertTrue(filter.isMatch(serviceCall));
		
		condition = new Condition(Field.PROBLEM, Relation.CONTAINS);
		condition.setText("Problem");
		conditions.add(condition);
		filter.setConditions(conditions);
		assertTrue(filter.isMatch(serviceCall));
	}
}
