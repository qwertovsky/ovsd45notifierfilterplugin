package com.qwertovsky.ovsd45notifier.plugin.filter;

import java.io.IOException;

import org.apache.log4j.BasicConfigurator;
import org.apache.log4j.Logger;
import org.eclipse.swt.layout.FillLayout;
import org.eclipse.swt.widgets.Display;
import org.eclipse.swt.widgets.Shell;
import org.junit.Test;


public class FormView
{
	@Test
	public void show() throws IOException
	{
		Display display = new Display();
		Shell shell = new Shell(display);
		shell.setLayout(new FillLayout());
		shell.setSize(100,100);
		shell.open();
		
		FilterPlugin plugin = new FilterPlugin();
		Logger logger = Logger.getLogger("test");
		BasicConfigurator.configure();
		plugin.setLogger(logger);
		
		FiltersForm filtersForm = new FiltersForm();
		filtersForm.show();
		
		while (!shell.isDisposed())
		{
			if (!display.readAndDispatch())
				display.sleep();
		}
		Shell[] shells = Display.getDefault().getShells();
		
		for(Shell s:shells)
			s.dispose();
		display.dispose();
		
	}
}
