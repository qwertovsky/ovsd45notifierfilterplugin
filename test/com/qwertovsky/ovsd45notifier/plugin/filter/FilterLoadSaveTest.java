package com.qwertovsky.ovsd45notifier.plugin.filter;

import java.util.ArrayList;
import java.util.List;

import org.apache.log4j.BasicConfigurator;
import org.apache.log4j.Logger;
import org.junit.BeforeClass;
import org.junit.Test;

public class FilterLoadSaveTest
{
	@BeforeClass
	public static void beforeClass()
	{
		FilterPlugin plugin = new FilterPlugin();
		Logger logger = Logger.getLogger("test");
		BasicConfigurator.configure();
		plugin.setLogger(logger);
		
	}
	
	// -------------------------------------------
	@Test
	public void testSave()
	{
		Filter filter = new Filter();
		filter.setInpact(Inpact.ANY_CONDITION);
		
		FilterService.addFilter(filter);
		
		List<Condition> conditions = new ArrayList<Condition>();
		
		Condition condition = new Condition(Field.CALLER, Relation.EQUAL);
		condition.setText("John Doe");
		conditions.add(condition);
		condition = new Condition(Field.CALLER, Relation.NOT_EQUAL);
		condition.setText("John Doe2");
		conditions.add(condition);
		filter.setConditions(conditions);
		
		FilterService.save();
	}

}
