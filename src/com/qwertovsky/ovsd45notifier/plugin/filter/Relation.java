package com.qwertovsky.ovsd45notifier.plugin.filter;

public enum Relation
{
	CONTAINS (0, "содержит")
	, NOT_CONTAINS (1, "не содержит")
	, EQUAL (2, "совпадает")
	, NOT_EQUAL (3, "не совпадает")
	, START_WITH (4, "начинается с")
	, END_WITH (5, "заканчивается на");
	
	private int key;
	private String name;
	
	private Relation(int key, String name)
	{
		this.key = key;
		this.name = name;
	}
	
	public String toString()
	{
		return name;
	}
	
	public static Relation valueOf(int key)
	{
		Relation[] r = values();
		for(int i = 0; i < r.length; i++)
		{
			if(r[i].key == key)
				return r[i];
		}
		return null;
	}
	
	public int getKey()
	{
		return key;
	}
	
	
}
