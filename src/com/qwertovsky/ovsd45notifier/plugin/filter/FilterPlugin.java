package com.qwertovsky.ovsd45notifier.plugin.filter;


import java.util.List;

import org.apache.log4j.Logger;
import org.eclipse.swt.SWT;
import org.eclipse.swt.events.SelectionAdapter;
import org.eclipse.swt.events.SelectionEvent;
import org.eclipse.swt.widgets.Menu;
import org.eclipse.swt.widgets.MenuItem;

import com.qwertovsky.ovsd45notifier.plugin.Plugin;
import com.qwertovsky.ovsd45notifier.plugin.ServiceCall;


public class FilterPlugin implements Plugin
{
	private MenuItem menuItem = null;
	private String name = "Filter";
	private boolean enabled = true;
	
	public static Logger logger = Logger.getLogger(FilterPlugin.class.getName());
	
	public void createMenuItem(Menu parent)
	throws Exception
	{
		menuItem = new MenuItem(parent, SWT.CASCADE);
		menuItem.setText(name);
		Menu menu = new Menu(parent);
		menuItem.setMenu(menu);
		MenuItem miEnabled = new MenuItem(menu, SWT.CHECK);
		miEnabled.setText("Активен");
		miEnabled.setSelection(isEnabled());
		MenuItem miOptions = new MenuItem(menu, SWT.PUSH);
		miOptions.setText("Фильтры...");
		
		miEnabled.addSelectionListener(new SelectionAdapter()
			{
				public void widgetSelected(SelectionEvent event)
				{
					MenuItem item = ((MenuItem)event.getSource());
					setEnabled(item.getSelection());
					logger.info("Расширение " + getName()
							+ (isEnabled()?" включено":" выключено"));
				}
			});
		
		miOptions.addSelectionListener(new SelectionAdapter()
		{
			@Override
			public void widgetSelected(SelectionEvent arg0)
			{
				// open form
				FiltersForm filtersForm = new FiltersForm();
				filtersForm.show();
			}
		});
	}
	
	// -------------------------------------------
	public boolean processServiceCall(ServiceCall serviceCall)
	throws Exception
	{
		FilterPlugin.logger.debug("Обработка заявки " + serviceCall.ID() + " в фильтрах");
		List<Filter> filters = FilterService.getFilters();
		for(Filter filter:filters)
		{
			if(!filter.isEnabled())
				continue;
			boolean match = filter.isMatch(serviceCall);
			if(match)
			{
				FilterPlugin.logger.info("Заявка " + serviceCall.ID() + " подпала под фильтр '"
						+ filter.getName() + "'");
				filter.runCommands();
				boolean showNotification = filter.isShowNotification();
				if(!showNotification)
					FilterPlugin.logger.info("Для заявки " + serviceCall.ID()
							+ " уведомление не выводится");
				return showNotification;
			}
		}
		return true;
	}
	
	// -------------------------------------------
	private void setEnabled(boolean enable)
	{
		this.enabled = enable;
		menuItem.setSelection(isEnabled());
	}

	public boolean isEnabled()
	{
		return enabled;
	}

	public String getName()
	{
		return name;
	}

	@Override
	public void setLogger(Logger logger)
	{
//		FilterPlugin.logger = logger;
	}
	
}
