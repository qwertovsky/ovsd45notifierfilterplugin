package com.qwertovsky.ovsd45notifier.plugin.filter;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;

@XmlAccessorType(XmlAccessType.FIELD)
public class FilterAction
{
	@XmlAttribute
	private Action action;
	private String command;
	
	public void setAction(Action action)
	{
		this.action = action;
	}
	public Action getAction()
	{
		return action;
	}
	public void setCommand(String command)
	{
		this.command = command;
	}
	public String getCommand()
	{
		return command;
	}
}
