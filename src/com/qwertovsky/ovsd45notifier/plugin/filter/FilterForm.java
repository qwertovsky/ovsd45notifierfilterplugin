package com.qwertovsky.ovsd45notifier.plugin.filter;

import java.io.FileNotFoundException;
import java.util.ArrayList;
import java.util.List;

import org.eclipse.swt.SWT;
import org.eclipse.swt.custom.TableEditor;
import org.eclipse.swt.events.ModifyEvent;
import org.eclipse.swt.events.ModifyListener;
import org.eclipse.swt.events.SelectionAdapter;
import org.eclipse.swt.events.SelectionEvent;
import org.eclipse.swt.graphics.Color;
import org.eclipse.swt.graphics.Image;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.layout.RowLayout;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Combo;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Control;
import org.eclipse.swt.widgets.Display;
import org.eclipse.swt.widgets.Event;
import org.eclipse.swt.widgets.Label;
import org.eclipse.swt.widgets.Listener;
import org.eclipse.swt.widgets.Shell;
import org.eclipse.swt.widgets.Table;
import org.eclipse.swt.widgets.TableColumn;
import org.eclipse.swt.widgets.TableItem;
import org.eclipse.swt.widgets.Text;

public class FilterForm
{
	private Filter filter;

	private Shell shell;
	private Text nameT;
	
	private Button allRB;
	private Button anyRB;
	private Button allCallsRB;
	
	private Table conditionT;
	private Button okB;
	private Button cancelB;

	private TableColumn fieldColumn;
	private TableColumn relationColumn;
	private TableColumn textColumn;
	private TableColumn addColumn;
	private TableColumn deleteColumn;

	private TableEditor fieldEditor;
	private TableEditor relationEditor;;
	private TableEditor textEditor;
	private TableEditor addEditor;
	private TableEditor deleteEditor;
	
	private Table actionT;
	private TableColumn actionColumn;
	private TableColumn commandColumn;
	private TableColumn addActionColumn;
	private TableColumn deleteActionColumn;

	private TableEditor actionEditor;
	private TableEditor commandEditor;
	private TableEditor addActionEditor;
	private TableEditor deleteActionEditor;

	public FilterForm(Filter filter)
	{
		this.filter = filter;
		buildForm();
		try
		{
			fillForm();
		} catch (FileNotFoundException e)
		{
			e.printStackTrace();
		}
		addListeners();
	}

	// -------------------------------------------
	public void show()
	{
		shell.setModified(false);
		shell.open();
		while(!shell.isDisposed() && shell.isVisible())
		{
			if(!shell.getDisplay().readAndDispatch())
			{
				shell.getDisplay().sleep();
			}
		}
	}

	// -------------------------------------------
	private void buildForm()
	{
		shell = new Shell(Display.getDefault(), SWT.BORDER | SWT.TITLE | SWT.APPLICATION_MODAL);
		shell.setText("Правила фильтрации");
		shell.setSize(600, 400);
		Image icon = new Image(shell.getDisplay(), this.getClass().getResourceAsStream(
			"/com/qwertovsky/ovsd45notifier/resources/10.png"));
		shell.setImage(icon);
		GridLayout gridLayout = new GridLayout(4, false);
		gridLayout.marginTop = 5;
		gridLayout.marginLeft = 5;
		gridLayout.marginRight = 5;
		gridLayout.marginBottom = 5;
		gridLayout.marginWidth = 0;
		gridLayout.marginHeight = 0;
		gridLayout.verticalSpacing = 5;
		gridLayout.horizontalSpacing = 5;
		shell.setLayout(gridLayout);

		// name
		Label nameL = new Label(shell, SWT.NULL);
		nameL.setText("Имя фильтра:");
		GridData nameLGD = new GridData();
		nameLGD.grabExcessHorizontalSpace = false;
		nameL.setLayoutData(nameLGD);
		nameT = new Text(shell, SWT.NONE | SWT.BORDER);
		GridData nameTGD = new GridData();
		nameTGD.grabExcessHorizontalSpace = true;
		nameTGD.horizontalAlignment = GridData.FILL;
		nameTGD.horizontalSpan = 3;
		nameT.setLayoutData(nameTGD);

		// inpact for filter
		Composite radioGroup = new Composite(shell, SWT.NULL);
	    radioGroup.setLayout(new RowLayout());

	    allRB = new Button(radioGroup, SWT.RADIO);
	    allRB.setText("все условия");
	    anyRB = new Button(radioGroup, SWT.RADIO);
	    anyRB.setText("любое из условий");
	    allCallsRB= new Button(radioGroup, SWT.RADIO);
	    allCallsRB.setText("все заявки");
	    allRB.setSelection(true);
	    
	    GridData radioGD = new GridData();
		radioGD.grabExcessHorizontalSpace = true;
		radioGD.horizontalAlignment = GridData.FILL;
		radioGD.horizontalSpan = 4;
		radioGroup.setLayoutData(radioGD);

		// Table
		conditionT = new Table(shell, SWT.BORDER | SWT.MULTI
				| SWT.FULL_SELECTION | SWT.HIDE_SELECTION);
		conditionT.setHeaderVisible(false);
		conditionT.setLinesVisible(true);
		
		fieldColumn = new TableColumn(conditionT, SWT.NONE);
		relationColumn = new TableColumn(conditionT, SWT.NONE);
		textColumn = new TableColumn(conditionT, SWT.NONE);
		addColumn = new TableColumn(conditionT, SWT.NONE);
		deleteColumn = new TableColumn(conditionT, SWT.NONE);
		fieldColumn.setWidth(125);
		relationColumn.setWidth(125);
		textColumn.setWidth(280);
		addColumn.setWidth(20);
		deleteColumn.setWidth(20);

		fieldEditor = new TableEditor(conditionT);
		relationEditor = new TableEditor(conditionT);
		textEditor = new TableEditor(conditionT);
		addEditor = new TableEditor(conditionT);
		deleteEditor = new TableEditor(conditionT);

		fieldEditor.grabHorizontal = true;
		fieldEditor.minimumHeight = 20;
		relationEditor.grabHorizontal = true;
		relationEditor.minimumHeight = 20;
		textEditor.grabHorizontal = true;
		textEditor.minimumHeight = 20;
		addEditor.minimumHeight = 20;
		addEditor.minimumWidth = 20;
		deleteEditor.minimumHeight = 20;
		deleteEditor.minimumWidth = 20;

		GridData tableGD = new GridData();
		tableGD.horizontalAlignment = GridData.FILL;
		tableGD.verticalAlignment = GridData.FILL;
		tableGD.grabExcessHorizontalSpace = true;
		tableGD.grabExcessVerticalSpace = true;
		tableGD.horizontalSpan = 4;
		conditionT.setLayoutData(tableGD);

		// actions
		actionT = new Table(shell, SWT.BORDER | SWT.MULTI
				| SWT.FULL_SELECTION | SWT.HIDE_SELECTION);
		actionT.setHeaderVisible(false);
		actionT.setLinesVisible(true);
		
		actionColumn = new TableColumn(actionT, SWT.NONE);
		commandColumn = new TableColumn(actionT, SWT.NONE);
		addActionColumn = new TableColumn(actionT, SWT.NONE);
		deleteActionColumn = new TableColumn(actionT, SWT.NONE);
		actionColumn.setWidth(200);
		commandColumn.setWidth(335);
		addActionColumn.setWidth(20);
		deleteActionColumn.setWidth(20);

		actionEditor = new TableEditor(actionT);
		commandEditor = new TableEditor(actionT);
		addActionEditor = new TableEditor(actionT);
		deleteActionEditor = new TableEditor(actionT);

		actionEditor.grabHorizontal = true;
		actionEditor.minimumHeight = 20;
		commandEditor.grabHorizontal = true;
		commandEditor.minimumHeight = 20;
		addActionEditor.minimumHeight = 20;
		addActionEditor.minimumWidth = 20;
		deleteActionEditor.minimumHeight = 20;
		deleteActionEditor.minimumWidth = 20;

		GridData actionGD = new GridData();
		actionGD.horizontalAlignment = GridData.FILL;
		actionGD.verticalAlignment = GridData.FILL;
		actionGD.grabExcessHorizontalSpace = true;
		actionGD.grabExcessVerticalSpace = true;
		actionGD.horizontalSpan = 4;
		actionT.setLayoutData(actionGD);

		// buttons
		Composite ok_cancelC = new Composite(shell, SWT.NONE);
		RowLayout rl = new RowLayout();
		rl.pack = false;
		ok_cancelC.setLayout(rl);
		GridData ok_canselGD = new GridData();
		ok_canselGD.horizontalAlignment = GridData.FILL;
		ok_canselGD.grabExcessHorizontalSpace = true;
		ok_canselGD.horizontalSpan = 4;
		ok_cancelC.setLayoutData(ok_canselGD);
		
		okB = new Button(ok_cancelC, SWT.PUSH);
		okB.setText("OK");
		
		cancelB = new Button(ok_cancelC, SWT.PUSH);
		cancelB.setText("Отмена");
	}

	// ---------------------------------------
	private void fillForm() throws FileNotFoundException
	{
		if (filter == null)
		{
			// add new line
			addNewConditionLine(null);
			addNewActionLine(null);
			return;
		}
		String name = filter.getName();
		nameT.setText(name);
		
		Inpact inpact = filter.getInpact();
		switch (inpact)
		{
		case ALL_CALLS:
			allCallsRB.setSelection(true);
			break;
		case ANY_CONDITION:
			anyRB.setSelection(true);
			break;
		case ALL_CONDITIONS:
			allRB.setSelection(true);
			break;
		default:
			allRB.setSelection(true);
			break;
		}
		
		List<Condition> conditions = filter.getConditions();
		for(Condition condition:conditions)
		{
			addNewConditionLine(condition);
		}
		
		List<FilterAction> actions = filter.getActions();
		for(FilterAction action:actions)
		{
			addNewActionLine(action);
		}
	}

	// ---------------------------------------
	private void addNewConditionLine(Condition condition)
	{
		conditionT.deselectAll();
		TableItem item = new TableItem(conditionT, SWT.NONE);
		if(condition != null)
		{
			Field field = condition.getField();
			Relation relation = condition.getRelation();
			String text = condition.getText();
			item.setData("field", field);
			item.setText(0, field.toString());
			item.setData("relation", relation);
			item.setText(1, relation.toString());
			item.setText(2, text);
		}
		disposeEditors();
		createConditionEditors(item);
		conditionT.select(conditionT.getItemCount() - 1);
	}
	
	// ---------------------------------------
	private void addNewActionLine(FilterAction filterAction)
	{
		actionT.deselectAll();
		TableItem item = new TableItem(actionT, SWT.NONE);
		if(filterAction != null)
		{
			Action action = filterAction.getAction();
			String command = filterAction.getCommand();
			item.setData("action", action);
			item.setText(0, action.toString());
			item.setText(1, command);
		}
		disposeEditors();
		createActionEditors(item);
		actionT.select(actionT.getItemCount() - 1);
	}

	// ---------------------------------------
	private void disposeEditors()
	{
		Control oldEditor = fieldEditor.getEditor();
		if (oldEditor != null)
			oldEditor.dispose();
		oldEditor = relationEditor.getEditor();
		if (oldEditor != null)
			oldEditor.dispose();
		oldEditor = textEditor.getEditor();
		if (oldEditor != null)
			oldEditor.dispose();
		oldEditor = addEditor.getEditor();
		if (oldEditor != null)
			oldEditor.dispose();
		oldEditor = deleteEditor.getEditor();
		if (oldEditor != null)
			oldEditor.dispose();
		
		oldEditor = actionEditor.getEditor();
		if (oldEditor != null)
			oldEditor.dispose();
		oldEditor = commandEditor.getEditor();
		if (oldEditor != null)
			oldEditor.dispose();
		oldEditor = addActionEditor.getEditor();
		if (oldEditor != null)
			oldEditor.dispose();
		oldEditor = deleteActionEditor.getEditor();
		if (oldEditor != null)
			oldEditor.dispose();
	}

	// ---------------------------------------
	private void addListeners()
	{
		allCallsRB.addSelectionListener(new SelectionAdapter()
			{
				public void widgetSelected(SelectionEvent event)
				{
					if(allCallsRB.getSelection())
					{
						conditionT.setEnabled(false);
						disposeEditors();
					}
					else 
						conditionT.setEnabled(true);
				}
			});
		
		conditionT.addSelectionListener(new SelectionAdapter()
		{
			public void widgetSelected(SelectionEvent event)
			{
				disposeEditors();

				// new editors
				TableItem item = (TableItem) event.item;
				if (item == null)
					return;
				createConditionEditors(item);
			}
		});
		
		conditionT.addListener(SWT.MeasureItem, new Listener()
		{

			@Override
			public void handleEvent(Event event)
			{
				event.height = 20;
			}
		});
		
		conditionT.addListener(SWT.Resize, new Listener()
        {
            public void handleEvent(Event event)
            {
                textColumn.setWidth(conditionT.getClientArea().width - 125*2 - 20*2);
            }
        });
		
		actionT.addSelectionListener(new SelectionAdapter()
		{
			public void widgetSelected(SelectionEvent event)
			{
				disposeEditors();

				// new editors
				TableItem item = (TableItem) event.item;
				if (item == null)
					return;
				createActionEditors(item);
			}
		});
		
		actionT.addListener(SWT.MeasureItem, new Listener()
		{

			@Override
			public void handleEvent(Event event)
			{
				event.height = 20;
			}
		});
		
		actionT.addListener(SWT.Resize, new Listener()
        {
            public void handleEvent(Event event)
            {
                commandColumn.setWidth(actionT.getClientArea().width - 240);
            }
        });

		cancelB.addSelectionListener(new SelectionAdapter()
		{
			public void widgetSelected(SelectionEvent event)
			{
				shell.dispose();
			}
		});
		
		okB.addSelectionListener(new SelectionAdapter()
		{
			public void widgetSelected(SelectionEvent event)
			{
				if(filter == null)
					filter = new Filter();
				String name = nameT.getText();
				if(name == null || name.isEmpty())
				{
					nameT.setBackground(new Color(shell.getDisplay(), 255, 200, 200));
					return;
				}
				filter.setName(name);
				// inpact
				if(allRB.getSelection())
					filter.setInpact(Inpact.ALL_CONDITIONS);
				else if(anyRB.getSelection())
					filter.setInpact(Inpact.ANY_CONDITION);
				else filter.setInpact(Inpact.ALL_CALLS);
				
				// get conditions
				List<Condition> conditions = new ArrayList<Condition>();
				TableItem[] items = conditionT.getItems();
				for(int i=0; i<items.length;i++)
				{
					String text = items[i].getText(2);
					Field field = (Field) items[i].getData("field");
					Relation relation = (Relation) items[i].getData("relation");
					Condition condition = new Condition(field, relation);
					condition.setText(text);
					conditions.add(condition);
				}
				filter.setConditions(conditions);
				
				// get actions
				List<FilterAction> filterActions = new ArrayList<FilterAction>();
				items = actionT.getItems();
				for(int i = 0; i< items.length; i++)
				{
					Action action = (Action) items[i].getData("action");
					String command = items[i].getText(1);
					FilterAction filterAction = new FilterAction();
					filterAction.setAction(action);
					filterAction.setCommand(command);
					filterActions.add(filterAction);
				}
				filter.setActions(filterActions);
				
				shell.dispose();
			}
		});
	}

	// ---------------------------------------
	private void createConditionEditors(TableItem item)
	{
		Combo fieldC = new Combo(conditionT, SWT.VERTICAL | SWT.DROP_DOWN
				| SWT.BORDER | SWT.READ_ONLY);
		fieldC.setBackground(new Color(Display.getDefault(), 255, 255, 255));
		Field[] fields = Field.values();
		for (int i = 0; i < fields.length; i++)
		{
			fieldC.add(fields[i].toString(), fields[i].getKey());
		}
		fieldEditor.setEditor(fieldC, item, 0);

		Combo relationC = new Combo(conditionT, SWT.VERTICAL | SWT.DROP_DOWN
				| SWT.BORDER | SWT.READ_ONLY);
		relationC.setBackground(new Color(Display.getDefault(), 255, 255, 255));
		Relation[] relations = Relation.values();
		for (int i = 0; i < relations.length; i++)
		{
			relationC.add(relations[i].toString(), relations[i].getKey());
		}
		relationEditor.setEditor(relationC, item, 1);

		Text text = new Text(conditionT, SWT.BORDER);
		textEditor.setEditor(text, item, 2);

		Button addB = new Button(conditionT, SWT.PUSH);
		addB.setText("+");
		addB.pack();
		addEditor.setEditor(addB, item, 3);

		Button deleteB = new Button(conditionT, SWT.PUSH);
		deleteB.setText("-");
		deleteB.pack();
		deleteEditor.setEditor(deleteB, item, 4);

		fieldC.addModifyListener(new ModifyListener()
		{
			public void modifyText(ModifyEvent e)
			{
				Combo field = (Combo) fieldEditor.getEditor();
				fieldEditor.getItem().setText(0, field.getText());
				int index = field.getSelectionIndex();
				fieldEditor.getItem().setData("field", Field.valueOf(index));
			}
		});
		if (item.getText(0) == null || item.getText(0).equals(""))
			fieldC.select(0);
		else
			fieldC.setText(item.getText(0));

		relationC.addModifyListener(new ModifyListener()
		{
			public void modifyText(ModifyEvent e)
			{
				Combo relation = (Combo) relationEditor.getEditor();
				relationEditor.getItem().setText(1, relation.getText());
				int index = relation.getSelectionIndex();
				relationEditor.getItem().setData("relation", Relation.valueOf(index));
			}
		});
		if (item.getText(1) == null || item.getText(1).equals(""))
			relationC.select(0);
		else
			relationC.setText(item.getText(1));

		text.addModifyListener(new ModifyListener()
		{
			public void modifyText(ModifyEvent e)
			{
				Text text = (Text) textEditor.getEditor();
				textEditor.getItem().setText(2, text.getText());
			}
		});
		text.setText(item.getText(2));

		addB.addSelectionListener(new SelectionAdapter()
		{
			@Override
			public void widgetSelected(SelectionEvent arg0)
			{
				addNewConditionLine(null);
			}
		});

		deleteB.addSelectionListener(new SelectionAdapter()
		{
			@Override
			public void widgetSelected(SelectionEvent event)
			{
				disposeEditors();
				int index = conditionT.getSelectionIndex();
				System.out.println("Delete index:" + index);
				if (index < 0)
					return;
				conditionT.remove(index);
				conditionT.redraw();
			}
		});
		if (conditionT.getItemCount() == 1)
			deleteB.setEnabled(false);
	}
	
	// ---------------------------------------
	private void createActionEditors(TableItem item)
	{
		Combo actionC = new Combo(actionT, SWT.VERTICAL | SWT.DROP_DOWN
				| SWT.BORDER | SWT.READ_ONLY);
		actionC.setBackground(new Color(Display.getDefault(), 255, 255, 255));
		Action[] actions = Action.values();
		for (int i = 0; i < actions.length; i++)
		{
			actionC.add(actions[i].toString(), actions[i].getKey());
		}
		actionEditor.setEditor(actionC, item, 0);

		Text command = new Text(actionT, SWT.BORDER);
		commandEditor.setEditor(command, item, 1);

		Button addB = new Button(actionT, SWT.PUSH);
		addB.setText("+");
		addB.pack();
		addActionEditor.setEditor(addB, item, 2);

		Button deleteB = new Button(actionT, SWT.PUSH);
		deleteB.setText("-");
		deleteB.pack();
		deleteActionEditor.setEditor(deleteB, item, 3);

		actionC.addModifyListener(new ModifyListener()
		{
			public void modifyText(ModifyEvent e)
			{
				Combo action = (Combo) actionEditor.getEditor();
				actionEditor.getItem().setText(0, action.getText());
				int index = action.getSelectionIndex();
				actionEditor.getItem().setData("action", Action.valueOf(index));
				if(Action.valueOf(index) == Action.NOT_SHOW_NOTIFICATION)
					commandEditor.getEditor().setEnabled(false);
				else
					commandEditor.getEditor().setEnabled(true);
			}
		});
		if (item.getText(0) == null || item.getText(0).equals(""))
			actionC.select(0);
		else
			actionC.setText(item.getText(0));

		command.addModifyListener(new ModifyListener()
		{
			public void modifyText(ModifyEvent e)
			{
				Text text = (Text) commandEditor.getEditor();
				commandEditor.getItem().setText(1, text.getText());
			}
		});
		command.setText(item.getText(1));

		addB.addSelectionListener(new SelectionAdapter()
		{
			@Override
			public void widgetSelected(SelectionEvent arg0)
			{
				addNewActionLine(null);
			}
		});

		deleteB.addSelectionListener(new SelectionAdapter()
		{
			@Override
			public void widgetSelected(SelectionEvent event)
			{
				disposeEditors();
				int index = actionT.getSelectionIndex();
				System.out.println("Delete index:" + index);
				if (index < 0)
					return;
				actionT.remove(index);
				actionT.redraw();
			}
		});
		if (actionT.getItemCount() == 1)
			deleteB.setEnabled(false);
		if(actionC.getSelectionIndex() == 0)
			command.setEnabled(false);
		
	}
	
	// -------------------------------------------
	public Filter getFilter()
	{
		return filter;
	}

}
