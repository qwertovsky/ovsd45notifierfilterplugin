package com.qwertovsky.ovsd45notifier.plugin.filter;

import java.lang.reflect.Method;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlElement;

import com.qwertovsky.ovsd45notifier.plugin.ServiceCall;

@XmlAccessorType(XmlAccessType.FIELD)
public class Condition
{
	@XmlAttribute
	private Field field;
	@XmlAttribute
	private Relation relation;
	@XmlElement
	private String text;
	
	@SuppressWarnings("unused")
	private Condition()
	{
		
	}
	
	public Condition(Field field, Relation relation)
	{
		this.setField(field);
		this.setRelation(relation);
	}
	
	// -------------------------------------------
	public boolean isCorrespond(ServiceCall serviceCall)
	{
		Method m = getField().method();
		if(m == null)
			return false;
		Relation relation = getRelation();
		String text = getText();
		if(text == null || text.isEmpty())
			return false;
		String content = null;
		try
		{
			content = (String) m.invoke(serviceCall, new Object[0]);
		} catch (Exception e)
		{
			FilterPlugin.logger.error("Не удалось вызвать метод, оответствующий правилу фильтрации: "
					+ m.getName());
			return false;
		}
		if(content == null || content.isEmpty())
		{
			if(text.isEmpty() && relation.equals(Relation.EQUAL))
				return true;
			return false;
		}
		
		switch(relation)
		{
		case CONTAINS:
			if(content.contains(text))
				return true;
			break;
		case NOT_CONTAINS:
			if(!content.contains(text))
				return true;
			break;
		case EQUAL:
			if(content.equals(text))
				return true;
			break;
		case NOT_EQUAL:
			if(!content.equals(text))
				return true;
			break;
		case END_WITH:
			if(content.endsWith(text))
				return true;
			break;
		case START_WITH:
			if(content.startsWith(content))
				return true;
			break;
		default:
			return false;
		}
		
		return false;
	}
	
	// -------------------------------------------
	public void setField(Field field)
	{
		this.field = field;
	}

	public Field getField()
	{
		return field;
	}

	public void setRelation(Relation relation)
	{
		this.relation = relation;
	}

	public Relation getRelation()
	{
		return relation;
	}

	public void setText(String text)
	{
		this.text = text;
	}

	public String getText()
	{
		return text;
	}
}
