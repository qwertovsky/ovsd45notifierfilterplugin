package com.qwertovsky.ovsd45notifier.plugin.filter;

import java.io.FileNotFoundException;
import java.util.List;

import org.eclipse.swt.SWT;
import org.eclipse.swt.events.DisposeEvent;
import org.eclipse.swt.events.DisposeListener;
import org.eclipse.swt.events.SelectionAdapter;
import org.eclipse.swt.events.SelectionEvent;
import org.eclipse.swt.graphics.Image;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Display;
import org.eclipse.swt.widgets.Event;
import org.eclipse.swt.widgets.Listener;
import org.eclipse.swt.widgets.Shell;
import org.eclipse.swt.widgets.Table;
import org.eclipse.swt.widgets.TableColumn;
import org.eclipse.swt.widgets.TableItem;

public class FiltersForm
{
	Shell shell;
	Table filterTable;
	Button addB;
	Button editB;
	Button deleteB;
	
	TableColumn nameColumn;
	
	public FiltersForm()
	{
		buildForm();
		try
		{
			fillForm();
		} catch (FileNotFoundException e)
		{
			e.printStackTrace();
		}
		addListeners();
	}
	
	// -------------------------------------------
	public void show()
	{
		shell.open();
	}
	
	// -------------------------------------------
	private void buildForm()
	{
		shell = new Shell(Display.getDefault());
		shell.setText("Фильтры");
		shell.setSize(500, 200);
		Image icon = new Image(shell.getDisplay(), this.getClass().getResourceAsStream(
		    "/com/qwertovsky/ovsd45notifier/resources/10.png"));
	    shell.setImage(icon);
		GridLayout gridLayout = new GridLayout(2, false);
		gridLayout.marginTop = 5;
		gridLayout.marginLeft = 5;
		gridLayout.marginRight = 5;
		gridLayout.marginBottom = 5;
		gridLayout.marginWidth = 0;
		gridLayout.marginHeight = 0;
		gridLayout.verticalSpacing = 5;
		gridLayout.horizontalSpacing = 5;
		shell.setLayout(gridLayout);
		
		//Table
		filterTable = new Table(shell, SWT.CHECK | SWT.BORDER | SWT.V_SCROLL 
				| SWT.SINGLE | SWT.FULL_SELECTION);
		nameColumn = new TableColumn(filterTable, SWT.LEFT);
		nameColumn.setText("Наименование");
		nameColumn.pack();
		
		GridData tableGD = new GridData();
		tableGD.horizontalAlignment = GridData.FILL;
		tableGD.verticalAlignment = GridData.FILL;
		tableGD.grabExcessHorizontalSpace = true;
		tableGD.grabExcessVerticalSpace = true;
		tableGD.verticalSpan = 3;
		filterTable.setLayoutData(tableGD);
		
		//buttons
		addB = new Button(shell, SWT.PUSH);
		addB.setText("Добавить");
		GridData addBGD = new GridData();
		addBGD.horizontalAlignment = GridData.FILL;
		addB.setLayoutData(addBGD);
		
		editB = new Button(shell, SWT.PUSH);
		editB.setText("Редактировать");
		GridData editBGD = new GridData();
		editBGD.horizontalAlignment = GridData.FILL;
		editB.setLayoutData(editBGD);
		
		deleteB = new Button(shell, SWT.PUSH);
		deleteB.setText("Удалить");
		GridData deleteBGD = new GridData();
		deleteBGD.horizontalAlignment = GridData.FILL;
		deleteBGD.verticalAlignment = GridData.BEGINNING;
		deleteB.setLayoutData(deleteBGD);
	}
	
	// -------------------------------------------
	private void fillForm() throws FileNotFoundException
	{
		List<Filter> filters = FilterService.getFilters();
		for(Filter filter:filters)
		{
			TableItem item = new TableItem(filterTable, SWT.NONE);
			item.setText(0,filter.getName());
			item.setData(filter);
			item.setChecked(filter.isEnabled());
			
		}
		
	}
	
	// -------------------------------------------
	private void addListeners()
	{
		addB.addSelectionListener(new SelectionAdapter()
		{
			@Override
			public void widgetSelected(SelectionEvent event)
			{
				Filter filter = null;
				
				// open filter form
				FilterForm filterForm = new FilterForm(filter);
				filterForm.show();
				filter = filterForm.getFilter();
				if(filter != null)
				{
					TableItem item = new TableItem(filterTable, SWT.NULL);
					item.setText(0,filter.getName());
					item.setData(filter);
					item.setChecked(filter.isEnabled());
					FilterService.addFilter(filter);
				}
			}
		});
		
		editB.addSelectionListener(new SelectionAdapter()
			{
				public void widgetSelected(SelectionEvent event)
				{
					if(filterTable.getSelectionCount() == 0)
						return;
					TableItem[] selected = filterTable.getSelection();
					TableItem item = selected[0];
					Filter filter = null;
					filter = (Filter) item.getData();
					FilterForm filterForm = new FilterForm(filter);
					filterForm.show();
					item.setText(0,filter.getName());
				}
			}
		);
		
		deleteB.addSelectionListener(new SelectionAdapter()
			{
				public void widgetSelected(SelectionEvent event)
				{
					if(filterTable.getSelectionCount() == 0)
						return;
					TableItem[] selected = filterTable.getSelection();
					int index = filterTable.getSelectionIndex();
					TableItem item = selected[0];
					Filter filter = (Filter)item.getData();
					filterTable.remove(index);
					FilterService.removeFilter(filter);
				}
			}
		);
		
		filterTable.addListener(SWT.Selection, new Listener()
			{
				public void handleEvent(Event event)
				{
					if(event.detail == SWT.CHECK)
					{
						TableItem item = (TableItem) event.item;
						boolean checked = item.getChecked();
						Filter filter = (Filter) item.getData();
						filter.setEnabled(checked);
					}
				}
			});
		
		filterTable.addListener(SWT.Resize, new Listener()
        {
            public void handleEvent(Event event)
            {
                nameColumn.setWidth(filterTable.getClientArea().width);
            }
        });
		
		shell.addDisposeListener(new DisposeListener()
		{
			@Override
			public void widgetDisposed(DisposeEvent event)
			{
				FilterService.save();
			}
		});
	}
}
