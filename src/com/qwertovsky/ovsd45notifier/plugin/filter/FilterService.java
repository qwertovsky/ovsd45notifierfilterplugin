package com.qwertovsky.ovsd45notifier.plugin.filter;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.ArrayList;
import java.util.List;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.Marshaller;
import javax.xml.bind.Unmarshaller;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;


@XmlRootElement(name = "filters")
public class FilterService
{
	@XmlElement(name = "filter")
	private List<Filter> filters = new ArrayList<Filter>();
	
	private static FilterService filterService = null;
	
	public static List<Filter> getFilters()
	{
		if(filterService == null)
		{
			filterService = new FilterService(); 
			filterService.filters = loadFilters();
		}
		return filterService.filters;
	}
	
	// -------------------------------------------
	public static void addFilter(Filter filter)
	{
		if(filterService == null)
		{
			filterService = new FilterService(); 
			filterService.filters = loadFilters();
		}
		filterService.filters.add(filter);
	}
	
	// -------------------------------------------
	public static void removeFilter(Filter filter)
	{
		if(filterService == null)
		{
			filterService = new FilterService(); 
			filterService.filters = loadFilters();
		}
		filterService.filters.remove(filter);
	}
	
	// -------------------------------------------
	private static List<Filter> loadFilters()
	{
		List<Filter> list = new ArrayList<Filter>();
		// load from file
		File filtersFile = new File("conf/filters.xml");
		if(!filtersFile.exists())
			return list;
		InputStream is = null;
		try
		{
			is = new FileInputStream(filtersFile);
			JAXBContext context = JAXBContext.newInstance(FilterService.class);
			Unmarshaller um = context.createUnmarshaller();
			FilterService fs = (FilterService) um.unmarshal(is);
			list = fs.filters;
		} catch (Exception e)
		{
			FilterPlugin.logger.error(e.getMessage(), e);
		} finally
		{
			if(is != null)
				try
				{
					is.close();
				} catch (IOException e)
				{
					
				}
		}
		return list;
	}
	
	// -------------------------------------------
	public static void save()
	{
		File confDir = new File("conf");
		if(!confDir.exists())
			confDir.mkdir();;
		File filtersFile = new File("conf/filters.xml");
		OutputStream os = null;
		try
		{
			if(!filtersFile.exists())
				filtersFile.createNewFile();
			os = new FileOutputStream(filtersFile);
			JAXBContext context = JAXBContext.newInstance(FilterService.class);
			Marshaller m = context.createMarshaller();
			m.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, true);
			m.marshal(filterService, os);
			
		} catch (Exception e)
		{
			FilterPlugin.logger.error(e.getMessage(), e);
		}
		finally
		{
			if(os != null)
				try
				{
					os.close();
				} catch (IOException e)
				{
					
				}
		}
	}
	
	// -------------------------------------------
}
