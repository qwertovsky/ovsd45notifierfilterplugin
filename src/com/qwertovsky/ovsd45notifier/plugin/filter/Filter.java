package com.qwertovsky.ovsd45notifier.plugin.filter;

import java.io.IOException;
import java.util.List;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlElementWrapper;

import com.qwertovsky.ovsd45notifier.plugin.ServiceCall;

@XmlAccessorType(XmlAccessType.FIELD)
public class Filter
{
	@XmlAttribute(name = "inpact")
	private Inpact inpact = Inpact.ALL_CONDITIONS;
	@XmlAttribute(name = "name")
	private String name;
	@XmlAttribute(name = "enabled")
	private boolean enabled = true;
	@XmlElementWrapper(name="conditions")
	@XmlElement(name="condition")
	private List<Condition> conditions;
	@XmlElementWrapper(name="actions")
	@XmlElement(name="action")
	private List<FilterAction> actions;
	
	public Inpact getInpact()
	{
		return inpact;
	}
	public void setInpact(Inpact inpact)
	{
		this.inpact = inpact;
	}
	public String getName()
	{
		return name;
	}
	public void setName(String name)
	{
		this.name = name;
	}
	public List<Condition> getConditions()
	{
		return conditions;
	}
	public void setConditions(List<Condition> conditions)
	{
		this.conditions = conditions;
	}
	public void setEnabled(boolean enabled)
	{
		this.enabled = enabled;
	}
	public boolean isEnabled()
	{
		return enabled;
	}
	public void setActions(List<FilterAction> actions)
	{
		this.actions = actions;
	}
	public List<FilterAction> getActions()
	{
		return actions;
	}
	
	// -------------------------------------------
	protected boolean isMatch(ServiceCall serviceCall)
	{
		Inpact inpact = getInpact();
		FilterPlugin.logger.debug(inpact);
		if(inpact == Inpact.ALL_CALLS)
			return true;
		
		List<Condition> conditions = getConditions();
		if(conditions.isEmpty())
			return false;
		
		boolean match = false;
		if(inpact == Inpact.ALL_CONDITIONS)
			match = true;
		for(Condition condition:conditions)
		{
			boolean correspond = condition.isCorrespond(serviceCall);
			FilterPlugin.logger.debug("Condition (" 
					+ condition.getField() + ", "
					+ condition.getRelation() + ", '"
					+ condition.getText() +"'): " + correspond);
			if(correspond && inpact == Inpact.ANY_CONDITION)
				return true;
			if(!correspond && inpact == Inpact.ALL_CONDITIONS)
				return false;
		}
		return match;
	}
	
	// -------------------------------------------
	public boolean isShowNotification()
	{
		List<FilterAction> fas = getActions();
		if(fas.isEmpty())
			return true;
		
		for(FilterAction fa:fas)
		{
			if(fa.getAction() == Action.NOT_SHOW_NOTIFICATION)
				return false;
		}
		return true;
	}
	
	// -------------------------------------------
	public void runCommands()
	{
		List<FilterAction> fas = getActions();
		if(fas.isEmpty())
			return;
		
		for(FilterAction fa:fas)
		{
			if(fa.getAction() == Action.RUN_PROGRAM)
			{
				String command = fa.getCommand();
				try
				{
					Runtime.getRuntime().exec("cmd /c start " + command);
				} catch (IOException e)
				{
					FilterPlugin.logger.error(e.getMessage(), e);
				}
			}
		}
	}
}
