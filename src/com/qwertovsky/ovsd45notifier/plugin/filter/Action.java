package com.qwertovsky.ovsd45notifier.plugin.filter;


public enum Action
{
	NOT_SHOW_NOTIFICATION(0, "не показывать уведомление")
	, RUN_PROGRAM(1, "запустить программу");

	private String name;
	private int key;

	Action(int key, String name)
	{
		this.name = name;
		this.key = key;
	}

	public String toString()
	{
		return name;
	}

	public static Action valueOf(int key)
	{
		Action[] a = values();
		for (int i = 0; i < a.length; i++)
		{
			if (a[i].key == key)
				return a[i];
		}
		return null;
	}

	public int getKey()
	{
		return key;
	}
}
