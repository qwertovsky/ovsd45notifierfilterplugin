package com.qwertovsky.ovsd45notifier.plugin.filter;

import java.lang.reflect.Method;

import com.qwertovsky.ovsd45notifier.plugin.ServiceCall;

public enum Field
{
	CALLER(0, "personName", "заявитель")
	, DESCRIPTION(1, "description","краткое описание")
	, PROBLEM(2, "problem","проблема");
	
	private String methodName;
	private String name;
	private int key;
	
	Field(int key, String methodName, String name)
	{
		this.methodName = methodName;
		this.name = name;
		this.key = key;
	}
	
	public String toString()
	{
		return name;
	}
	
	public static Field valueOf(int key)
	{
		Field[] f = values();
		for(int i = 0; i < f.length; i++)
		{
			if(f[i].key == key)
				return f[i];
		}
		return null;
	}
	
	public Method method()
	{
		Method method = null;
		try
		{
			method = ServiceCall.class.getMethod(methodName, new Class[0]);
		} catch (NoSuchMethodException e)
		{
			FilterPlugin.logger.error("Не существует метод, соответствующий правилу фильтрации");
			return null;
		} catch (SecurityException e)
		{
			FilterPlugin.logger.error("Запрещен доступ к методу, соответствующему правилу фильтрации");
			return null;
		}
		return method;
	}
	
	public int getKey()
	{
		return key;
	}
}
